#endcoding:utf-8
from django.db import models

# Create your models here.

class Director(models.Model):
	nombreDirector = models.CharField(max_length = 100)
	pais = models.CharField(max_length = 100)

	def __unicode__(self):
		return self.nombreDirector

class Actor(models.Model):
	nombreActor = models.CharField(max_length = 100)
	pais = models.CharField(max_length = 100)
	def __unicode__(self):
		return self.nombreActor

class Pelicula(models.Model):
	nombre = models.CharField(max_length = 100)
	anio = models.DateField()
	sinopsis = models.TextField()
	director = models.ManyToManyField(Director, related_name = 'director')
	actor = models.ManyToManyField(Actor, related_name = 'actor')
	
	def __unicode__(self):
		return self.nombre
	
class Critica(models.Model):
	pelicula = models.ForeignKey(Pelicula, related_name = 'critica')
	critica = models.TextField()
	def __unicode__(self):
		return unicode(self.pelicula) + ' ' + unicode(self.critica)