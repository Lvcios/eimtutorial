from django.shortcuts import render
from peliculas.models import *
from serializers import *
from rest_framework import viewsets, generics, filters

class DirectorViewSet(viewsets.ModelViewSet):
	queryset = Director.objects.all()
	serializer_class = DirectorSerializer

class PeliculaViewSet(viewsets.ModelViewSet):
	queryset = Pelicula.objects.all()
	serializer_class = PeliculaSerializer
	filter_backends = (filters.DjangoFilterBackend,)
	filter_fields = ('director',)

class ActorViewSet(viewsets.ModelViewSet):
	queryset = Actor.objects.all()
	serializer_class = ActorSerializer

class CriticaPeliculaViewSet(viewsets.ModelViewSet):
	queryset = Critica.objects.all()
	serializer_class = CriticaSerializer
	