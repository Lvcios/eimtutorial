from rest_framework import serializers
from peliculas.models import *

class DirectorSerializer(serializers.ModelSerializer):
	class Meta:
		model = Director
		fields = ('nombreDirector','pais')

class ActorSerializer(serializers.ModelSerializer):
	class Meta:
		model = Actor
		fields = ('nombreActor','pais')

class PeliculaSerializer(serializers.ModelSerializer):
	actor = serializers.HyperlinkedRelatedField(many = True, read_only = True, view_name = 'actor-detail')
	director = serializers.HyperlinkedRelatedField(many = True, read_only = True, view_name = 'director-detail')
	critica = serializers.HyperlinkedRelatedField(many = True, read_only = True, view_name = 'critica-detail')
	class Meta:
		model = Pelicula
		fields = ('nombre','anio','sinopsis','director','actor','critica')

class CriticaSerializer(serializers.ModelSerializer):
	criticas = serializers.RelatedField(many = True)
	class Meta:
		model = Critica
		fields = ('pelicula','critica','pelicula')