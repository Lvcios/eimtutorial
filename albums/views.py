from django.shortcuts import render
from serializers import *
from rest_framework import viewsets
from albums.models import *
# Create your views here.

class AlbumViewSet(viewsets.ModelViewSet):
	queryset = Album.objects.all()
	serializer_class = AlbumSerializer
	
class TrackViewSet(viewsets.ModelViewSet):
	queryset = Track.objects.all()
	serializer_class = TrackSerializer