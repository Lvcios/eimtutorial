from rest_framework import serializers
from albums.models import *

class AlbumSerializer(serializers.ModelSerializer):
	tracks = serializers.HyperlinkedRelatedField(many=True, read_only=True, view_name='track-detail')
	class Meta:
		model = Album
		fields = ('album_name', 'artist', 'tracks')
		
class TrackSerializer(serializers.ModelSerializer):
	class Meta:
		model = Track
		fields = ('album','order','title','duration')