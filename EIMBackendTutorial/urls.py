from django.conf.urls import patterns, include, url
from rest_framework import routers
from peliculas.views import *
from albums.views import *
from django.contrib import admin
admin.autodiscover()

router = routers.SimpleRouter()
router.register(r'directores', DirectorViewSet)
router.register(r'actores', ActorViewSet)
router.register(r'peliculas', PeliculaViewSet)
router.register(r'criticas', CriticaPeliculaViewSet)
router.register(r'albums',AlbumViewSet)
router.register(r'tracks',TrackViewSet)

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'EIMBackendTutorial.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'api/v1/', include(router.urls)),
    url(r'api-auth/', include('rest_framework.urls', namespace='rest_framework'))
)
